FROM ubuntu
RUN adduser --system --group bot && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        gosu \
        libclass-dbi-sqlite-perl \
        libpoe-component-client-http-perl \
        libpoe-component-irc-perl \
        perl
VOLUME /data
WORKDIR /data
COPY greybot.pl /bin/bot
RUN chmod +x /bin/bot
CMD ["bash", "-c", "chown -R bot: /data; exec gosu bot bot"]
