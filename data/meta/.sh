lhunath 1216716931 learn Don't use extensions for your scripts. Executables shouldn't have extensions. Just like you run 'ls' instead of 'ls.o', you should run 'myscript', not 'myscript.sh'. In any case, putting 'sh' extensions on *bash* scripts is retarded.
r00t|vaio 1229778702 forget
r00t|vaio 1229778730 learn Don't use extensions for your scripts. Executables shouldn't have extensions. Just like you run 'ls' instead of 'ls.elf', you should run 'myscript', not 'myscript.sh'. In any case, putting 'sh' extensions on *bash* scripts is retarded.
lhunath 1242593756 forget
lhunath 1242593804 learn Don't use extensions for your scripts. Executables shouldn't have extensions. Just like you run 'ls' instead of 'ls.elf', you should run 'myscript' or './myscript', not 'myscript.sh'. In any case, putting 'sh' extensions on *bash* scripts (which are *not* sh scripts) is retarded.
lhunath 1304082927 forget
lhunath 1304083296 learn Don't use extensions for your scripts. Scripts define new commands that you can run, and commands are generally not given extensions. Also: bash script are *not* sh script (so don't use .sh) and the extension will only cause dependencies headaches if the script gets rewritten in another language.
geirha 1309093814 forget
geirha 1309093820 learn Don't use extensions for your scripts. Scripts define new commands that you can run, and commands are generally not given extensions. Also: bash scripts are *not* sh script (so don't use .sh) and the extension will only cause dependencies headaches if the script gets rewritten in another language.
neurolysis 1318091714 forget
neurolysis 1318091714 learn Don't use extensions for your scripts. Scripts define new commands that you can run, and commands are generally not given extensions. Also: bash scripts are *not* sh scripts (so don't use .sh) and the extension will only cause dependencies headaches if the script gets rewritten in another language.
e36freak 1350591131 forget
e36freak 1350591145 learn Don't use extensions for your scripts. Scripts define new commands that you can run, and commands are generally not given extensions. Do you run ls.elf? Also: bash scripts are *not* sh scripts (so don't use .sh) and the extension will only cause dependencies headaches if the script gets rewritten in another language. See http://www.talisman.org/~erlkonig/documents/commandname-extensions-considered-harmful
